# --------------------------------------------- #
# Plugin Name           : Scanner Telegram Bot    #
# Author Name           : Tom               #
# File Name             : main.py               #
# --------------------------------------------- #

import config
import re
import requests
import telebot
import validators

bot = telebot.TeleBot(config.token)
bot.set_webhook()
table_name = "users"

@bot.message_handler(commands=['sc'])
def handle_text(message):
    bot.send_chat_action(message.chat.id, 'typing')
    command = message.text.split(' ')
    if len(command) <= 1:
        bot.send_message(message.chat.id, "Please input the contract address")
        return
    contract_address = command[1]
    telegram_link = 'NOT FOUND'
    twitter_link = 'NOT FOUND'
    web_link = 'NOT FOUND'
    medium_link = 'NOT FOUND'
    discord_link = 'NOT FOUND'

    telegram_pattern = r'\b(?:https?://(?:www\.)?t(?:elegram)?\.me/\w+|t(?:elegram)?\.me/\w+)\b'
    twitter_pattern = r'\b(?:https?://(?:www\.)?(?:twitter\.com|x\.com)/\w+|(?:twitter\.com|x\.com)/\w+)\b'
    web_pattern = r'\b(?:https?://(?:www\.)?(?!(?:twitter|telegram|medium|discord|t\.me|telegram\.me|discord\.gg|x\.com)/)\w+\.\w{2,}|www\.(?!(?:twitter|telegram|medium|discord|x\.com)/)\w+\.\w{2,})\b'
    medium_pattern = r'\b(?:https?://(?:www\.)?medium\.com/\w+|medium\.com/\w+)\b'
    discord_pattern = r'\b(?:https?://(?:www\.)?discord\.gg/\w+|discord\.gg/\w+)\b'

    if not config.strategy_live:
        bot.send_message(message.chat.id, config.texts[
            'airdrop_start'],
                            parse_mode='Markdown', disable_web_page_preview=True,reply_markup=open_link())
    else:
        scan_social = scan_social_links(contract_address)
        token_name = scan_social['token_name']
        social_links = scan_social['social_links']
        if social_links is None:
            bot.send_message(message.chat.id, 'The contract address is invalid', parse_mode='Markdown', disable_web_page_preview=True)
            return None
        for link in social_links:
            is_valid = validators.url(link)
            if is_valid:
                telegram_links = re.findall(telegram_pattern, link) if telegram_link == 'NOT FOUND' else []
                telegram_link = telegram_links[0] if len(telegram_links)>0 else telegram_link

                twitter_links = re.findall(twitter_pattern, link) if twitter_link == 'NOT FOUND' else []
                twitter_link = twitter_links[0] if len(twitter_links)>0 else twitter_link

                web_links = re.findall(web_pattern, link) if web_link == 'NOT FOUND' else []
                web_link = web_links[0] if len(web_links)>0 else web_link

                medium_links = re.findall(medium_pattern, link) if medium_link == 'NOT FOUND' else []
                medium_link = medium_links[0] if len(medium_links)>0 else medium_link

                discord_links = re.findall(discord_pattern, link) if discord_link == 'NOT FOUND' else []
                discord_link = discord_links[0] if len(discord_links)>0 else discord_link
        ether_link = 'https://etherscan.io/address/'+contract_address
        scan_paragraph = f'🌐*Social Scraper Report* 🌐 \n*Token name*: [{token_name}]({ether_link}) \n*CA*: '+contract_address+'\n\n*Telegram* ➡️: '+telegram_link+' \n*Twitter*     🔵: '+twitter_link+'\n*Website*   🌐: '+web_link+' \n*Medium*   📄: '+medium_link+' \n*Discord*    🐽: '+discord_link

        bot.send_message(message.chat.id, scan_paragraph, parse_mode='Markdown', disable_web_page_preview=True)
        return None

@bot.callback_query_handler(func=lambda call: True)
def callback_query(call):
    if call.data == "cancel_input":
        bot.delete_message(chat_id=call.message.chat.id, message_id=call.message.message_id)
        bot.send_message(call.message.chat.id, '✅ Operation canceled.', reply_markup=defaultkeyboard)
        bot.clear_step_handler_by_chat_id(chat_id=call.message.chat.id)

def get_contract_source_code(contract_address):
    # Use an API to fetch the contract source code
    etherscan_api_key = config.etherscan_api_key
    endpoint = f'?module=contract&action=getsourcecode&address={contract_address}&apikey={etherscan_api_key}'
    try:
        response = requests.get(config.base_url+endpoint)
        data = response.json()
        if 'result' in data and data['result']:
            return False if isinstance(data['result'], str) else data['result'][0]['SourceCode']
        else:
            return False
    except requests.RequestException as e:
        raise ValueError(f"Error fetching contract source code: {e}")

def get_social_link(source_code):
    social_link_pattern = r'(?:(?<!\S)(?:(?:https?://)?(?:www\.)?)?[a-zA-Z0-9:/]+\.[a-zA-Z]{2,4}(?:/[^\s]*)?)'
    social_links = re.findall(social_link_pattern, source_code)
    return social_links

def extract_token_name(contract_source_code):
    token_name_pattern = r'(?i)\bstring\s+private\s+constant\s+_name\s*=\s*unicode\s*"([^"]+)"\s*;'
    match = re.search(token_name_pattern, contract_source_code)
    if match:
        return match.group(1)
    return "Token name not found in the contract source code."

def scan_social_links(contract_address):
    try:
        source_code = get_contract_source_code(contract_address)
        if not source_code:
            return {
            "token_name": "",
            "social_links": None}
        social_links = get_social_link(source_code)
        token_name = extract_token_name(source_code)
        return {
            "token_name": token_name,
            "social_links": social_links}
    except ValueError as e:
        return {
            "token_name": "",
            "social_links": ""}

bot.enable_save_next_step_handlers(delay=2)
bot.load_next_step_handlers()
bot.polling()
